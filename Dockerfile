FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7

RUN pip install requests
RUN pip install pymongo

ENV LISTEN_PORT 80

EXPOSE 80

COPY ./app /app