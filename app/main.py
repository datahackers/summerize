
from flask import Flask
from flask import request
from flask import jsonify
import logging
import os
import pythonScript
import requests
import json
import pymongo
import getDataFromRC

import pprint

app = Flask(__name__)
urlKeywordImage = 'http://localhost:5000/test'



class LoggingMiddleware(object):
    def __init__(self, app):
        self._app = app

    def __call__(self, environ, resp):
        errorlog = environ['wsgi.errors']
        pprint.pprint(('REQUEST', environ), stream=errorlog)

        def log_response(status, headers, *args):
            pprint.pprint(('RESPONSE', status, headers), stream=errorlog)
            return resp(status, headers, *args)

        return self._app(environ, log_response)

@app.route("/<idArticle>", methods=['GET'])
def default(idArticle):
    # result = testUpload()
    # return result
    # data = request.get_json()

    data = getDataFromRC.getData(idArticle)
    if(data == False):
        return jsonify({'success': False, 'message': 'the article cant be summerized'}), 410
    pythonScript.generateSumUP(data['articleId'], data['url'], data['theme'], data['title'], data['body'])
    payload = {'articleId': data['articleId']}
    r = requests.get(urlKeywordImage, params=payload)
    r = requests.get(urlKeywordImage)
    result = {}
    result['success'] = True
    result['message'] = 'Résumé génerer et continuation du pipeline'
    return jsonify(result)





def testUpload():
    uri = os.environ['MONGO_URI']
    client = pymongo.MongoClient(uri)
    db = client.cwise
    data = {}
    data['hello']='test'
    db.contents.insert_one(data)
    return 'yo'
    return 'ok'

@app.route('/test', methods=['POST', 'GET'])
def other():
    print('le call est bien fait')
    return "Le test fonctionne"


if __name__ == "__main__":
    # Only for debugging while developing
    if (os.environ.get('LISTEN_PORT') == None):
        app.run(host='0.0.0.0', debug=True, port=8080)
    else:
        app.run(host='0.0.0.0', debug=True, port=os.environ.get('LISTEN_PORT'))


