import os
import requests
import json
import re
from bs4 import BeautifulSoup


def getData(idArticle):
    clientKey = 'Client-Key ' + str(os.environ.get('RC_API_KEY'))
    if (clientKey == 'Client-Key None'):
        clientKey = 'Client-Key bf9ac6d8-9ad8-4124-a63c-7b7bdf22a2ee'
    # print(clientKey)
    url = 'https://services.radio-canada.ca/hackathon/neuro/v1/news-stories/' + \
        str(idArticle)
    # print(url)
    headers = {'Authorization': clientKey}
    r = requests.get(url, headers=headers)
    if(r.status_code != 200):
        return False
    r.encoding = 'utf-8'
    # the article title : 
    data = r.json()
    title = data['title']
    theme = data['themeTag']
    body = processBody(data['body']['html'])

    print(body)


def processBody(json):
  text = removeTag(json)
  text = removeLireAussi(text)
  return text

def removeLireAussi(text):
    matchObj = re.match('(.*)(À lire aussi .*)', text)
    return matchObj.group(1)

def removeTag(text):
    withouthtml = BeautifulSoup(text.encode('utf-8'), "lxml").text
    withoutWhiteSpace = " ".join(withouthtml.split())
    return withoutWhiteSpace



text = "<p>Plus de 402 000 Canadiens âgés de 65 ans et plus souffraient d'une forme de démence, et 61 d'entre eux recevaient des soins à domicile en 2015-2016, selon le rapport de l'ICIS.<br><br>Ainsi, environ 270 000 personnes âgées étaient soignées à la maison dans la période couverte par le rapport, les enfants (58 ) et les conjoints ou conjointes (32 ) prenant le plus souvent le rôle d'aidants naturels.<br><br>Disant publier pour la première fois des données sur la démence, l'ICIS a déterminé que 45  des aides familiales vivent de la détresse reliée à leur rôle, comparativement à 26  de celles fournissant des soins à des proches aînés n'ayant pas une forme de démence.<br><br>Les aidants naturels non rémunérés de personnes âgées expérimentant une forme de démence passent en moyenne 26&nbsp;heures par semaine à fournir des soins, comparativement à 17&nbsp;heures pour les aidants naturels d'aînés non affligés par un trouble neurologique évolutif.<br><br>Kathleen Morris, vice-présidente à la recherche et à l'analyse pour l'ICIS, a souligné que les aidants naturels des personnes âgées atteintes de démence «&nbsp;font face à de grands défis, surtout à mesure que la maladie progresse&nbsp;».</p>\n<blockquote>\n<p class=\"quote\">Nous espérons que notre rapport suscitera des discussions sur la façon de mieux soutenir ces personnes, qui jouent un rôle essentiel dans les systèmes de santé.</p>\n<footer>\nKathleen Morris, vice-présidente à la recherche et à l'analyse pour l'ICIS\n</footer></blockquote>\n<p>Selon l'ICIS, les aidants naturels ont «&nbsp;assumé environ 1,4 milliard de dollars en coûts directs&nbsp;» en 2016.<br><br>La détresse se traduit par une forme d'épuisement professionnel ou un sentiment d'être dépassé par les exigences du rôle d'aidant naturel, a indiqué Tracy Johnson, directrice à l'analyse du système de santé et questions émergentes.<br><br>À mesure que la démence s'aggrave, a-t-elle ajouté, il peut y avoir non seulement des pertes de mémoire et d'autres fonctions cognitives, mais aussi des changements de comportement et de personnalité.<br><br>«&nbsp;Les aidants naturels peuvent se trouver à participer à un éventail d'activités, notamment l'entretien ménager, la préparation des repas et le transport, ainsi que les soins personnels comme le bain et l'habillage. Les aidants naturels des personnes atteintes de démence doivent également assumer d'autres responsabilités, comme offrir un soutien affectif, gérer les comportements difficiles et s'assurer que l'être cher respecte sa médication&nbsp;», indique le rapport.<br><br>«&nbsp;Ce que ce rapport indique est que probablement, plus d'aide est nécessaire&nbsp;», a fait valoir Mme Johnson.</p>\n<div class=\"framed\">\n<h2>À lire aussi&nbsp;:</h2>\n<ul>\n<li><a title href=\"https://ici.radio-canada.ca/premiere/emissions/les-eclaireurs/segments/chronique/36098/aidants-naturels-repit-repos-importance\">Aidants naturels : l’importance de s'accorder une pause ou un répit</a></li>\n<li><a title href=\"https://ici.radio-canada.ca/nouvelle/1109145/alexa-personne-agee-assistant-etude\">L’intelligence artificielle à la rescousse de gens atteints de démence</a></li>\n<li><a title href=\"https://ici.radio-canada.ca/nouvelle/1091110/aidants-naturels-recit-vie-enfant-handicape-alzheimer\">Proches aidants, un engagement pour la vie</a></li>\n</ul>\n</div>\n<p></p>"

getData('1113639')
# removeTag(text)
