import sys
import json
import pymongo
import summerize


def initiateJSON():
    data = {}
    data['isSummerized'] = False
    data['hasKeyword'] = False
    data['hasImage'] = False
    data['hasAllElement'] = False
    data['isRendered'] = False
    data['isUserValidated'] = False
    data['isAvailable'] = False
    return data


def fillJSON(articleId, url, theme, title, body, summerizedBody):
    data = initiateJSON()
    # data = json.load(open('data.json', 'r'))
    data['articleId'] = articleId
    data['url'] = url
    data['theme'] = theme
    data['title'] = title
    data['body'] = body
    data['isSummerized'] = True
    data['urlArticle'] = url
    data['summerizedBody'] = summerizedBody
    data["idArticle"] = '13545'
    return data


def prepDataAndUpload(articleId, url, theme, title, body):
    # TODO: pretraitement du body pour retirer blaise html
    summerizedBody = summerize.summerize(body)
    data = fillJSON(articleId, url, theme, title, body, summerizedBody)
    # UPLOAD TO THE DB
    # TODO use env variable
    uri = "mongodb://cwise:password@DB:27017/cwise"
    client = pymongo.MongoClient(uri)
    db = client.cwise
    db.contents.insert(data)
    return True


def testUpload():
    uri = "mongodb://cwise:password@DB:27017/cwise"
    client = pymongo.MongoClient(uri)
    db = client.cwise
    db.contents.insert(data)
    return ok